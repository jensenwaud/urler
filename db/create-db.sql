
------------------------------------------------------------
-- "THE BEERWARE LICENSE" (Revision 42):
-- Anders Jensen-Waud wrote this code. As long as you retain this 
-- notice, you can do whatever you want with this stuff. If we
-- meet some day, and you think this stuff is worth it, you can
-- buy me a beer in return.
------------------------------------------------------------


DROP INDEX IF EXISTS url_access_idx;
DROP INDEX IF EXISTS url_code_idx;
DROP TABLE IF EXISTS click;
DROP TABLE IF EXISTS url; 
DROP TYPE IF EXISTS url_type; 
DROP TYPE IF EXISTS access_type;

CREATE TYPE url_type AS ENUM ('url', 'paste');
CREATE TYPE access_type AS ENUM ('public', 'private');

CREATE TABLE url ( 
    id serial NOT NULL PRIMARY KEY,
    code varchar(255) NOT NULL,
    created timestamptz NOT NULL DEFAULT now(),
    updated timestamptz NOT NULL DEFAULT now(), 
    url_type url_type NOT NULL, -- url, text, ...
    val text NOT NULL,
    creator_ip text,
    access access_type NOT NULL DEFAULT 'public'
);

CREATE UNIQUE INDEX CONCURRENTLY url_code_idx ON url (code);
CREATE INDEX url_access_idx ON url(access);

CREATE TABLE click (
    id SERIAL NOT NULL PRIMARY KEY,
    url_id int NOT NULL, 
    created timestamptz NOT NULL DEFAULT now(),
    ip text, -- ip address
    FOREIGN KEY(url_id) REFERENCES url(id)
);

