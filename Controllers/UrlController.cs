/*
 * ------------------------------------------------------------
 * "THE BEERWARE LICENSE" (Revision 42):
 * Anders Jensen-Waud wrote this code. As long as you retain this 
 * notice, you can do whatever you want with this stuff. If we
 * meet some day, and you think this stuff is worth it, you can
 * buy me a beer in return.
 * ------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using urler.Models;
using urler.Services;

namespace urler.Controllers
{
    [ApiController]
    [Route("u")]
    public class UrlController : Controller
    {
        private readonly ILogger<UrlController>_log;
        private readonly IUrlRepository _repository; 
        private readonly IConfiguration _config; 
        
        public UrlController(ILogger<UrlController> log, IUrlRepository repository, IConfiguration configuration)
        {
            _log = log;
            _repository = repository;
            _config = configuration;
        }
    

        [HttpPost]
        public async Task<ActionResult<UrlerResult>> HandlePost()
        {
            using (StreamReader reader = new StreamReader(Request.Body, System.Text.Encoding.UTF8))
            {  
                var res = new UrlerResult();
                try {
                    
                    res.Status = "OK";
                    var url = new Url();
                    url.Code = KeyGenerator.GetUniqueKey(10);
                    url.Created = DateTime.Now;
                    url.Updated = DateTime.Now;
                    url.CreatorIp = Request.HttpContext.Connection.RemoteIpAddress.ToString();
                    url.UrlType = "url";
                    url.Val = await reader.ReadToEndAsync();
                    url.Access = "public";
                    _repository.Save(url);

                    
                    // Generate URL:
                    res.Url = string.Format("{0}/{1}", _config.GetValue<string>("url"), url.Code);
                    res.Status = "OK";
                    // return await reader.ReadToEndAsync();
                    return CreatedAtAction("HandlePost", new { id = url.Code }, url);
                } 
                catch (Exception ex) { 
                    _log.LogError("PasteController.HandlePost exception caught: {0}", ex);
                    res.Status = "ERR";
                    res.Url = "Caught errro when trying to process your input. Please try again.";
                    return BadRequest();
                }
            } 
        }
    }
}
