﻿/*
 * ------------------------------------------------------------
 * "THE BEERWARE LICENSE" (Revision 42):
 * Anders Jensen-Waud wrote this code. As long as you retain this 
 * notice, you can do whatever you want with this stuff. If we
 * meet some day, and you think this stuff is worth it, you can
 * buy me a beer in return.
 * ------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using urler.Models;
using urler.Services;

namespace urler.Controllers
{
    public class HomeController : Controller
    {

        private readonly ILogger<HomeController> _log;
        private readonly IUrlRepository _repository;
        private readonly IClickRepository _clickrepo;

        private readonly IConfiguration _config;


        public HomeController(ILogger<HomeController> log, IUrlRepository repository, IClickRepository clickrepo, IConfiguration configuration)
        {
            _log = log;
            _repository = repository;
            _clickrepo = clickrepo;
            _config = configuration;
        }


        public async Task<IActionResult> ShowPaste(string code)
        {
            var paste = await _repository.GetByCode(code);
            ViewData["paste"] = paste;
            ViewData["numClicks"] = await _clickrepo.CountByUrl(paste);
            return View();
        }

        public IActionResult About() {
            return View();
        }

        public IActionResult CreateUrl([FromForm]string url, [FromForm] string paste, [FromForm] string access) { 
            var u = new Url();
            u.Access = (access != null && access == "on") ? "private" : "public";
            u.Code = KeyGenerator.GetUniqueKey();
            u.Created = DateTime.Now;
            u.Updated = DateTime.Now;
            u.CreatorIp  = Request.HttpContext.Connection.RemoteIpAddress.ToString();
            u.UrlType = (url != null && url.Length > 0) ? "url" : "paste";
            if (u.UrlType == "paste") { 
                u.Val = paste;
                _repository.Save(u);
                return RedirectToAction("ShowPaste", "Home", new { Code = u.Code });
            } else { 
                u.Val = url;
                _repository.Save(u);
                return RedirectToAction("Index");
            }    
        }

        public IActionResult Submit() { 
            return View();
        }

        public async Task<IActionResult> Index()
        {
            var code = string.Empty;
            if (Request.QueryString.Value.Length > 0)
            {
                code = Request.QueryString.Value.Remove(0, 1);
            }
            if (code.Length == 0)
            {
                ViewData["mode"] = "list";
                try {
                    var urls = await _repository.GetRecent("public", "url", 0, 10);
                    var pastes = await _repository.GetRecent("public", "paste", 0, 10);
                    ViewData["urls"] = urls;
                    ViewData["pastes"] = pastes;
                    ViewData["base"] = _config.GetValue<string>("url");
                    return View();    
                }
                catch (Exception ex) {
                    _log.LogWarning("Caught exception: {0}", ex);
                    return StatusCode(200);
                }
            }
            else
            {
                var u = await _repository.GetByCode(code);
                var click = new Click();
                click.Created = DateTime.Now;
                click.Ip = Request.HttpContext.Connection.RemoteIpAddress.ToString();
                click.UrlId = u.Id;
                _clickrepo.Save(click);

                if (u.UrlType == "url")
                {
                    return Redirect(u.Val);
                }
                else
                {
                    return RedirectToAction("ShowPaste", "Home", new { Code = code });
                }
            }
        }
    }
}
