/*
 * ------------------------------------------------------------
 * "THE BEERWARE LICENSE" (Revision 42):
 * Anders Jensen-Waud wrote this code. As long as you retain this 
 * notice, you can do whatever you want with this stuff. If we
 * meet some day, and you think this stuff is worth it, you can
 * buy me a beer in return.
 * ------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using urler.Models;

namespace urler.Services
{
    public interface IClickRepository
    {
        
        Task<long> CountByUrl(Url u); 

        long Save(Click c);
    }
}
