/*
 * ------------------------------------------------------------
 * "THE BEERWARE LICENSE" (Revision 42):
 * Anders Jensen-Waud wrote this code. As long as you retain this 
 * notice, you can do whatever you want with this stuff. If we
 * meet some day, and you think this stuff is worth it, you can
 * buy me a beer in return.
 * ------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using urler.Models;

namespace urler.Services
{
    public interface IUrlRepository
    {
        Task<Url> GetByCode(string code);

        Task<IEnumerable<Url>> GetRecent(string access, string type, int offset, int limit);

        long Save(Url url);

        void Delete(Url url);
    }
}
