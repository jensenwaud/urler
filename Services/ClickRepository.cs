/*
 * ------------------------------------------------------------
 * "THE BEERWARE LICENSE" (Revision 42):
 * Anders Jensen-Waud wrote this code. As long as you retain this 
 * notice, you can do whatever you want with this stuff. If we
 * meet some day, and you think this stuff is worth it, you can
 * buy me a beer in return.
 * ------------------------------------------------------------
 */

using System;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using urler.Models;
using Npgsql;
using Dapper;
using Dapper.FluentMap;
using System.Collections.Generic;

namespace urler.Services
{
    public class ClickRepository : IClickRepository
    {

        private readonly IConfiguration _config; 

        public ClickRepository(IConfiguration config) {
            _config = config; 
        }
        
        public async Task<long> CountByUrl(Url u)
        {
            using (IDbConnection conn = Connection) { 
                string q = "SELECT COUNT(id) FROM click WHERE url_id = @UrlId";
                conn.Open();
                var result = (await conn.QueryAsync<long>(q, new { UrlId = u.Id })).Single<long>();
                return result;
            }
        }

        public long Save(Click c)
        {
            using (IDbConnection dbConnection = Connection)
            {
                string sql = @"INSERT INTO click
                            (url_id, created, ip)
                            VALUES (@UrlId, @Created, @Ip); 
                            SELECT currval('click_id_seq');";
                var id = dbConnection.Query<long>(sql, c).Single<long>();
                
                c.Id = (long)id;
                return id;
            }
        }
        
        public IDbConnection Connection 
        {
            get {
                var s = _config.GetConnectionString("ConnStr");
                return new Npgsql.NpgsqlConnection(s); 
            }
        }
    }
}
