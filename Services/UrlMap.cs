/*
 * ------------------------------------------------------------
 * "THE BEERWARE LICENSE" (Revision 42):
 * Anders Jensen-Waud wrote this code. As long as you retain this 
 * notice, you can do whatever you want with this stuff. If we
 * meet some day, and you think this stuff is worth it, you can
 * buy me a beer in return.
 * ------------------------------------------------------------
 */
 
using Dapper.FluentMap.Mapping;
using urler.Models;

namespace urler.Services
{
    public class UrlMap : EntityMap<Url>
    {
        public UrlMap()
        {
            // Map property 'InvoiceID' to column 'Id'.
            Map(i => i.Id).ToColumn("id");
            Map(i => i.Code).ToColumn("code");
            Map(i => i.Created).ToColumn("created");
            Map(i => i.Updated).ToColumn("updated");
            Map(i => i.UrlType).ToColumn("url_type");
            Map(i => i.Val).ToColumn("val");
            Map(i => i.CreatorIp).ToColumn("creator_ip");

            // Ignore the 'Detail' and 'Items' properties when mapping.
            // Map(i => i.Detail).Ignore();
            // Map(i => i.Items).Ignore();
        }
    }

    public class ClickMap : EntityMap<Click>
    {
        public ClickMap()
        {
            // Map property 'InvoiceID' to column 'Id'.
            Map(i => i.Id).ToColumn("id");
            Map(i => i.UrlId).ToColumn("url_id");
            Map(i => i.Created).ToColumn("created");
            Map(i => i.Ip).ToColumn("ip");

            /* 
            Map(i => i.Code).ToColumn("code");
            Map(i => i.Created).ToColumn("created");
            Map(i => i.Updated).ToColumn("updated");
            Map(i => i.UrlType).ToColumn("url_type");
            Map(i => i.Val).ToColumn("val");
            Map(i => i.CreatorIp).ToColumn("creator_ip"); */

            // Ignore the 'Detail' and 'Items' properties when mapping.
            // Map(i => i.Detail).Ignore();
            // Map(i => i.Items).Ignore();
        }
    }


}