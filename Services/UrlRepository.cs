/*
 * ------------------------------------------------------------
 * "THE BEERWARE LICENSE" (Revision 42):
 * Anders Jensen-Waud wrote this code. As long as you retain this 
 * notice, you can do whatever you want with this stuff. If we
 * meet some day, and you think this stuff is worth it, you can
 * buy me a beer in return.
 * ------------------------------------------------------------
 */

using System;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using urler.Models;
using Npgsql;
using Dapper;
using Dapper.FluentMap;
using System.Collections.Generic;

namespace urler.Services
{
    public class UrlRepository : IUrlRepository
    {

        private readonly IConfiguration _config; 

        public UrlRepository(IConfiguration config) {
            _config = config; 
        }
        public void Delete(Url url)
        {
            throw new NotImplementedException();
        }

        public async Task<Url> GetByCode(string code)
        {
            using (IDbConnection conn = Connection) { 
                string q = @"SELECT id, 
                code, 
                created, 
                updated, 
                url_type::text AS UrlType, 
                val, 
                creator_ip, 
                access::text AS access 
                FROM url 
                WHERE code = @Code";
                conn.Open();
                var result = await conn.QueryAsync<Url>(q, new { Code = code });
                return result.FirstOrDefault();
            }

        }
        
        public long Save(Url item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                item.Val =  System.Web.HttpUtility.HtmlEncode(item.Val); // Security: Sanitise
                string sql = @"INSERT INTO url
                            (code, created, updated, url_type, val, creator_ip, access)
                            VALUES (
                                @Code, 
                                @Created, 
                                @Updated, 
                                @UrlType::url_type, 
                                @Val, 
                                @CreatorIp, 
                                @Access::access_type); 
                            SELECT currval('url_id_seq');";
                var id = dbConnection.Query<long>(sql, item).Single<long>();
                
                item.Id = (long)id;
                return id;
            }
        }

        public async Task<IEnumerable<Url>> GetRecent(string access, string type, int offset, int limit)
        {
            using (IDbConnection conn = Connection) { 
                string q = @"SELECT * FROM url WHERE
                     access::text = @Access AND url_type::text = @Type 
                     ORDER BY created DESC 
                     OFFSET @Offset LIMIT @Limit";
                conn.Open();
                var result = await conn.QueryAsync<Url>(q, new { Access = access, Type = type, Offset = offset, Limit = limit });
                return result;
            }
        }

        public IDbConnection Connection 
        {
            get {
                var s = _config.GetConnectionString("ConnStr");
                return new Npgsql.NpgsqlConnection(s); 
            }
        }
    }
}
