/*
 * ------------------------------------------------------------
 * "THE BEERWARE LICENSE" (Revision 42):
 * Anders Jensen-Waud wrote this code. As long as you retain this 
 * notice, you can do whatever you want with this stuff. If we
 * meet some day, and you think this stuff is worth it, you can
 * buy me a beer in return.
 * ------------------------------------------------------------
 */

using System;

namespace urler.Models
{
    public class Url
    {
        public long Id { get; set; } 
        public string Code { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
        public string UrlType { get; set; }

        public string Val { get; set; }
        public string CreatorIp { get; set; }

        public string Access { get; set; }
    }
}